package compressecpoint

import (
	"crypto/elliptic"
	"errors"
	"fmt"
	"math/big"
)

// Compress compress a point to a byte string
func Compress(curve elliptic.Curve, x, y *big.Int) []byte {
	if x.Sign() == 0 && y.Sign() == 0 {
		return []byte{0x00}
	}

	byteLen := (curve.Params().BitSize + 7) >> 3

	ret := make([]byte, 1+byteLen)
	if y.Bit(0) == 0 {
		ret[0] = 0x02
	} else {
		ret[0] = 0x03
	}

	xBytes := x.Bytes()
	copy(ret[byteLen-len(xBytes)+1:], xBytes)
	return ret
}

func DecompressA0(curve elliptic.Curve, data []byte) (x, y *big.Int, err error) {
	if data == nil {
		return nil, nil, errors.New("input is nil")
	}
	if len(data) == 0 {
		return nil, nil, errors.New("input is empty")
	}
	byteLen := (curve.Params().BitSize + 7) >> 3
	switch data[0] {
	case 0x00:
		if len(data) == 1 {
			return new(big.Int), new(big.Int), nil
		}
		return nil, nil, errors.New("input format is not valid, should be {0x00}")
	case 0x02, 0x03:
		{
			if len(data) != 1+byteLen {
				return nil, nil, fmt.Errorf("input length is not valid, should be %d", 1+byteLen)
			}
			x = new(big.Int).SetBytes(data[1:])

			// x³ + b
			x3 := new(big.Int).Mul(x, x)
			x3.Mul(x3, x)

			x3.Add(x3, curve.Params().B)
			x3.Mod(x3, curve.Params().P)

			y = new(big.Int).ModSqrt(x3, curve.Params().P)
			if y == nil {
				return nil, nil, errors.New("y is not exist")
			}
			if y.Bit(0) != uint(data[0]&0x01) {
				y.Sub(curve.Params().P, y)
			}
			return x, y, nil
		}
	case 0x04:
		x, y = elliptic.Unmarshal(curve, data)
		if x == nil || y == nil {
			return nil, nil, errors.New("point is not on curve")
		}
		return x, y, nil
	default:
		return nil, nil, errors.New("first byte is not 0x00 / 0x02 / 0x03 / 0x04")
	}
}

// Decompress decompress a byte string to a point
func Decompress(curve elliptic.Curve, data []byte) (x, y *big.Int, err error) {
	if data == nil {
		return nil, nil, errors.New("input is nil")
	}
	if len(data) == 0 {
		return nil, nil, errors.New("input is empty")
	}
	byteLen := (curve.Params().BitSize + 7) >> 3
	switch data[0] {
	case 0x00:
		if len(data) == 1 {
			return new(big.Int), new(big.Int), nil
		}
		return nil, nil, errors.New("input format is not valid, should be {0x00}")
	case 0x02, 0x03:
		{
			if len(data) != 1+byteLen {
				return nil, nil, fmt.Errorf("input length is not valid, should be %d", 1+byteLen)
			}
			x = new(big.Int).SetBytes(data[1:])

			// x³ - 3x + b
			x3 := new(big.Int).Mul(x, x)
			x3.Mul(x3, x)

			threeX := new(big.Int).Lsh(x, 1)
			threeX.Add(threeX, x)

			x3.Sub(x3, threeX)
			x3.Add(x3, curve.Params().B)
			x3.Mod(x3, curve.Params().P)

			y = new(big.Int).ModSqrt(x3, curve.Params().P)
			if y == nil {
				return nil, nil, errors.New("y is not exist")
			}
			if y.Bit(0) != uint(data[0]&0x01) {
				y.Sub(curve.Params().P, y)
			}
			return x, y, nil
		}
	case 0x04:
		x, y = elliptic.Unmarshal(curve, data)
		if x == nil || y == nil {
			return nil, nil, errors.New("point is not on curve")
		}
		return x, y, nil
	default:
		return nil, nil, errors.New("first byte is not 0x00 / 0x02 / 0x03 / 0x04")
	}
}
